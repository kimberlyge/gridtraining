/**
 * Call everything from here
 */

$(document).ready(function(){

	$('.js-galleryToggle').click(function(event){
		event.preventDefault();
		$('.js-gallery').magnificPopup('open');
	});

	$('.js-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true
		}
	});

});
